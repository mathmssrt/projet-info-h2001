package com.example.checkers

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.RectF

class Pawn(
    var isBlack: Boolean,
    var view: BoardView,
    var containerCase: Case,
) {
    var image: Int
    var bitmap: Bitmap
    var radius = (view.caseSize * 0.8f) / 2
    var x = containerCase.x // Centré sur la case pour commencer
    var y = containerCase.y
    var pawnRect = RectF(x - radius, y - radius, x + radius, y + radius)

    init {
        if (isBlack) {
            image = view.resources.getIdentifier("black_pawn", "drawable", view.getContext().getPackageName())
        } else {
            image = view.resources.getIdentifier("white_pawn", "drawable", view.getContext().getPackageName())
        }
        bitmap = BitmapFactory.decodeResource(view.getResources(), image)
    }

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(bitmap, null, pawnRect, null)
    }

    fun updatePawnSize() {
        radius = (view.caseSize * 0.8f) / 2
    }

    fun initializePawn() {
        x = containerCase.x
        y = containerCase.y
        pawnRect.set(x - radius, y - radius, x + radius, y + radius)
    }

    // Actualise les coordonnées du pion, appeler cette fonction pour déplacer le pion sur sa nouvelle coordonnée (sert aussi à initialiser les pions)
    fun updatePawn() {
        //pawnRect.offset(dx, dy)
        //x = pawnRect.centerX() // nouvelles coordonnées après l'offset
        //y = pawnRect.centerY()

    }
}