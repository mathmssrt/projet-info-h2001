package com.example.checkers

import android.graphics.RectF
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.drawable.Drawable

class Case(
    var leftCoordinate: Float,
    var topCoordinate: Float,
    var rightCoordinate: Float,
    var bottomCoordinate: Float,
    var isBlack: Boolean,
    var view: BoardView,
    val id: Int,
) {
    val caseRect = RectF(leftCoordinate, topCoordinate, rightCoordinate, bottomCoordinate)
    var image: Int
    var bitmap: Bitmap
    var x = caseRect.centerX()
    var y = caseRect.centerY()

    // Lié à la classe et non une instance
    companion object {

        // Contient toutes les instances de cette classe
        val casesMap = mutableMapOf<Int, Case>()
    }

    init {
        if (isBlack) {
            image = view.resources.getIdentifier("black_case", "drawable", view.getContext().getPackageName())
        } else {
            image = view.resources.getIdentifier("white_case", "drawable", view.getContext().getPackageName())
        }
        bitmap = BitmapFactory.decodeResource(view.getResources(), image)

        // Ajoute l'instance lors de sa création
        casesMap[id] = this
    }

    fun draw(canvas: Canvas) {
        canvas.drawBitmap(bitmap, null, caseRect, null)
    }

    // Actualise les coordonnées de la case (RectF())
    fun updateCase() {
        caseRect.set(leftCoordinate, topCoordinate, rightCoordinate, bottomCoordinate)
        x = caseRect.centerX()
        y = caseRect.centerY()
    }
}