package com.example.checkers

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView

class BoardView @JvmOverloads
constructor(
    context: Context,
    attributes: AttributeSet? = null,
    defStyleAttr: Int = 0,
): SurfaceView(context, attributes, defStyleAttr), SurfaceHolder.Callback, Runnable {
    lateinit var canvas: Canvas
    lateinit var thread: Thread
    var drawing = false
    var isFirstTime = true
    var screenWidth = 0f
    var screenHeight = 0f
    var boardSize = 8
    var caseSize = 0f
    val boardMatrix = createMatrix(boardSize)
    val pawnsMatrix = deepCopy(boardMatrix)
    var playerBlack: Player
    var playerWhite: Player

    init {
        modifyBoardMatrix(boardSize)
        createBoard()
        playerBlack = Player(true, this)
        playerWhite = Player(false, this)
        playerBlack.createPawns()
        playerWhite.createPawns()
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)

        screenWidth = width.toFloat()
        screenHeight = height.toFloat()
        caseSize = screenWidth / boardSize

        // Mise à jour des dimensions des composants
        updateBoardDimensions() // Initialise le plateau (Cases) en mm temps
        playerBlack.updatePawnsSize()
        playerWhite.updatePawnsSize()

        // Initialisation
        if (isFirstTime) {
            playerBlack.initializePawns()
            playerWhite.initializePawns()
            isFirstTime = false
        }
    }

    override fun run() {
        while (drawing) {
            draw()
        }
    }

    fun resume() {
        drawing = true
        thread = Thread(this)
        thread.start()
    }

    fun pause() {
        drawing = false
        thread.join()
    }

    fun draw() {
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()

            //for (key in casesMap.keys) casesMap[key]?.draw(canvas)
            for (i in 1..boardSize * boardSize) {
                Case.casesMap[i]?.draw(canvas) // Plus rapide
            }
            playerBlack.drawPawns(canvas)
            playerWhite.drawPawns(canvas)

            holder.unlockCanvasAndPost(canvas)
        }
    }

    // Crée une matrice n*n contenant des 0 et 1 (0 = case blanche, 1 = case noire)
    private fun createMatrix(n: Int): MutableList<MutableList<Int>> {
        var matrix = mutableListOf<MutableList<Int>>()
        var startValue = 0
        for (i in 1..n) {
            if (i % 2 == 0) startValue = 1
            var line = mutableListOf<Int>(startValue)
            for (j in 0..n - 1) {
                if (line.size < boardSize) {
                    if (line[j] == 0) line.add(1) else line.add(0)
                }
            }
            matrix.add(line)
            startValue = 0
        }
        return matrix
    }

    fun modifyBoardMatrix(n: Int) {
        val mid = (n / 2)
        val whiteStart = n - 2
        for (i in 0..n - 1) {
            if (i + 1 in mid until whiteStart) {
                for (j in 0..pawnsMatrix[i].size - 1) {
                    pawnsMatrix[i][j] = 0
                }
            } else if (i + 1 >= whiteStart) {
                for (j in 0..pawnsMatrix[i].size - 1) {
                    if (pawnsMatrix[i][j] == 1) {
                        pawnsMatrix[i][j] = 2
                    }
                }
            }
        }
    }

    fun deepCopy(matrix: MutableList<MutableList<Int>>): MutableList<MutableList<Int>> {
        val copy = mutableListOf<MutableList<Int>>()
        for (i in 0..matrix.size - 1) {
            var line = mutableListOf<Int>()
            for (j in 0..matrix[i].size - 1)  {
                line.add(matrix[i][j])
            }
            copy.add(line)
        }
        return copy
    }

    // Crée les cases en fonction de boardMatrix et les ajoutes à casesMap
    fun createBoard() {
        var counter = 1 // numéro de la case = id de la case
        for (i in 0..boardMatrix.size - 1) {
            for (j in 0..boardMatrix[i].size - 1) {
                var black = true
                if (boardMatrix[i][j] == 0) black = false
                var createdCase = Case(j * caseSize, 0 + (i * caseSize), (j + 1) * caseSize, 0 + ((i + 1) * caseSize), black, this, counter)
                counter++
            }
        }
    }

    // Met à jour les dimensions des cases
    fun updateBoardDimensions() {
        var counter = 1 // numéro de la case
        for (i in 0..boardMatrix.size - 1) {
            for (j in 0..boardMatrix[i].size - 1) {
                Case.casesMap[counter]?.leftCoordinate = j * caseSize
                Case.casesMap[counter]?.topCoordinate = 0 + (i * caseSize)
                Case.casesMap[counter]?.rightCoordinate = (j + 1) * caseSize
                Case.casesMap[counter]?.bottomCoordinate = 0 + ((i + 1) * caseSize)
                Case.casesMap[counter]?.updateCase()
                counter++
            }
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int,
                                width: Int, height: Int) {
    }
    override fun surfaceCreated(holder: SurfaceHolder) {
    }
    override fun surfaceDestroyed(holder: SurfaceHolder) {
    }
}
