package com.example.checkers

import android.graphics.Canvas

class Player(
    var isBlack: Boolean,
    var view: BoardView,
) {
    val playerPawns = mutableListOf<Pawn>()

    // Initialise les coordonnées des pions
    fun initializePawns() {
        for (i in 0..playerPawns.size - 1) {
            playerPawns[i].initializePawn()
        }
    }

    fun updatePawnsSize() {
        for (i in 0..playerPawns.size - 1) {
            playerPawns[i].updatePawnSize()
        }
    }

    fun drawPawns(canvas: Canvas) {
        for (i in 0..playerPawns.size - 1) {
            playerPawns[i].draw(canvas)
        }
    }

    // Crée les pions en fonction de pawnsMatrix et les ajoute dans leurs listes respectives
    fun createPawns() {
        var counter = 1 // Sert à savoir sur quelle case le pion est placé
        var value = 2
        if (isBlack) value = 1
        for (i in 0..view.pawnsMatrix.size - 1) {
            for (j in 0..view.pawnsMatrix[i].size - 1) {
                if (view.pawnsMatrix[i][j] == value) {
                    var createdPawn = Case.casesMap[counter]?.let { Pawn(isBlack, view, it) }
                    if (createdPawn != null) {
                        playerPawns.add(createdPawn)
                    }
                }
                counter++
            }
        }
    }
}