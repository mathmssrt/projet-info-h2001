package com.example.checkers

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    lateinit var boardView: BoardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        boardView = findViewById<BoardView>(R.id.Board)
    }

    override fun onResume() {
        super.onResume()
        boardView.resume()
    }

    override fun onPause() {
        super.onPause()
        boardView.pause()
    }
}